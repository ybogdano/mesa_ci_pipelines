#!/usr/bin/env python

import os
import git
import argparse
import shutil
import tempfile
import textwrap

from fetch_branchdirs import fetch_branchdirs

def push_localdirs(reset=False, dryrun=True, verbose=True, lds2push=[]):
    local_repo = git.Repo()
    upstream_remote = local_repo.remote("upstream")

    local_repo.remote("upstream").pull("main", rebase=True)
    fetch_branchdirs(reset=reset, brs2fetch=lds2push) # bootstrap worktrees and bring them up to date

    commit_msg = local_repo.commit().message
    for br_basename in os.listdir("localdirs"):
        local_dir = f"localdirs/{br_basename}"
        wt_dir = f"worktrees/{br_basename}"

        if lds2push and (br_basename not in lds2push):
            continue

        copy_and_push_fromdir(local_dir, wt_dir, "upstream", commit_msg, dryrun=dryrun, verbose=verbose)

    local_commits = list(local_repo.iter_commits(f"upstream/main..{local_repo.commit().hexsha}"))
    if local_commits:
        print(f"pushing {len(local_commits)} commited change(s) on your local repo to upstream...")
    if verbose:
        for c in local_commits:
            print('=' * 80)
            print(local_repo.git.show(c))

    if dryrun:
        print("WARN: dryrun set so local repo commits won't actually be pushed")
        local_repo.remote("upstream").push("--dry-run")
    else:
        local_repo.remote("upstream").push()


def copy_and_push_fromdir(src_dir, repo_dir, remote_name, commit_message, dryrun=True, verbose=True):
    dryrun_alert = ""

    if dryrun:
        dryrun_alert = "(WARN DRYRUN; no changes will actually be pushed): "

    try:
        repo = git.Repo(repo_dir)
    except Exception as error:
        print(error)
        print(f"failed to create repo object for {repo_dir}. skipping...")
        return

    if repo.is_dirty():
        print(f"uncommited changes to {repo_dir}, skipping...")
        return

    try:
        shutil.copytree(f"{src_dir}/",
                        repo_dir,
                        ignore=shutil.ignore_patterns(".*", "*.orig", "#*#", "*~"),
                        dirs_exist_ok=True)
    except Exception as error:
        print(error)
        print(f"could not copy contents of {src_dir} to {repo_dir}. skipping...")
        return

    if not repo.is_dirty():
        print(f"no changes between {src_dir} and {repo_dir}, skipping...")
        return

    if verbose:
        print(f"changes made to {repo_dir}:")
        print(repo.commit().diff())

    try:
        repo.git.add("--all")
        repo.index.commit(commit_message)
        print(f"{dryrun_alert}changes in {src_dir} committed successfully to {repo_dir}. Pushing...")
        if verbose:
            print(f"{dryrun_alert}changes commited to {repo_dir} in {repo.commit().hexsha}:")
            print(repo.git.show())
    except Exception as error:
        print(error)
        print(f"WARN: {repo_dir} cannot be pushed possibly due to a merge conflict, skipping...")
        return

    try:
        if dryrun:
            repo.remote(remote_name).push("--dry-run")
            print(f"dryrun push of {repo_dir} successful!")
            repo.head.reset("HEAD^", index=True, working_tree=True, hard=True)
            print(f"reset {repo_dir} after dryrun")
        else:
            repo.remote(remote_name).push()
            print(f"pushed {repo_dir} to remote {remote_name}")
    except Exception as error:
        print(error)
        print(f"WARN: {repo_dir} cannot be pushed possibly due to a merge conflict, skipping...")
        return


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description=
                                        textwrap.fill("copy contents of (by default ALL) "
                                                      "localdirs to corresponding "
                                                      "branchdirs and push changes upstream, "
                                                      "then push commited changes to the "
                                                      "local repo to upstream/main", width=80),
                                        formatter_class=argparse.RawTextHelpFormatter)
    argparser.add_argument("--reset",
                           action="store_true",
                           help="reset worktrees before pushing to upstream branches")
    argparser.add_argument("--dryrun",
                          action=argparse.BooleanOptionalAction,
                          default=True,
                          help=
                           textwrap.fill("NB: this option is always on by default. "
                                         "To turn off you must use the '--no-dryrun' option. "
                                         "don't actually push or update the index in the worktrees", width=80))
    argparser.add_argument("--verbose",
                           action=argparse.BooleanOptionalAction,
                           default=True,
                           help="Set by default. --no-verbose unsets.")
    argparser.add_argument("--localdirs",
                           nargs="+",
                           default=[],
                           help="list of localdirs to push upstream "
                           "eg. --localdirs foo bar foobar...")

    args = argparser.parse_args()

    push_localdirs(reset=args.reset, dryrun=args.dryrun, verbose=args.verbose, lds2push=args.localdirs)
