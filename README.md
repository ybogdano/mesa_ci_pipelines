How to use these scripts:

1) run ```python fetch_branchdirs.py``` to setup worktrees. You will use these to transmit changes from localdirs to upstream.
   localdirs: staging directories that contain changes you'd like to make to their corresponding upstream branches.
   (eg. localdir dev_ngcortes maps to upstream branch dev_ngcortes)
   wortrees: generally you shouldn't modify these directly. These are where changes are copied from localdirs and pushed to the upstream remote

2) create a jinja2 compatible template in the templates directory that will be a valid Jenkinsfile when the template is applied

3) Add an entry to jenkinsfile_branch_map.yaml mapping your template to a corresponding branch on upstream (eg. dev_tpalli)

4) run ```python apply_templates.py --stage```
   to generate a jenkinsfile using your template, copy it to its subdir in the localdirs directory,
   and stage the generated files for committing to this branch (via ```git add```).
   NB: You can also exclude the ```--stage``` option if you'd like to stage the files manually.

5) commit your work with ```git commit```, then verify all files look correct with ```git show```

6) run ```python push_localdirs.py``` to see what changes will be made when you push everything to upstream.

7) if everything looked good in 6), run ```python push_branchdirs.py --no-dryrun```
